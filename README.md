# Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? 

Dos expertos realizan una comparación sobre la evolución de la programación en las últimas dos décadas del pasado siglo y la actualidad.
 
Mapa conceptual con PlantUML
```plantuml
@startmindmap
+[#Salmon] Evaluación en la forma de programar
++[#goldenrod] Programación en los 80's
+++[#peru] Limitación dada por el hardware
++++[#rosybrown] Rendimiento como prioridad
+++_ Existian mas
++++[#peru] Diversidad de arquitecturas
+++++_ Provocando la
++++++[#rosybrown] Necesidad de comprender el hardware
+++++++_ Existiendo una
++++++++[#tan] Relación directa entre programador y hardware
++++++++_ Se trabaja con
+++++++++[#tan] Lenguajes de bajo nivel
++++++++++_ Como
+++++++++++[#lightcoral] Ensamblador
++++++++_ Entonces
+++++++++[#tan] El funcionamiento depende del programador
++[#goldenrod] Programación actual
+++_ Se cuenta con un
++++[#peru] Hardware más potente
+++++_ Por lo que se toma la
++++++[#peru] Claridad del código como prioridad
++++++_ Ahora
+++++++[#peru] No es necesario conocer bien el hardware
++++++++_ Ya que existen
+++++++++[#tan] Plataformas genéricas (VM)
++++++++++[#lightcoral] Lenguajes parcialmente compilados
+++++++++++[#coral] Java
+++++++++++[#coral] Kotlin
+++++++++++[#coral] Scala
+++++++++_ Trabajan a través de
++++++++++[#tan] Multiples capas de abstracción
+++++++++++_ Lo que genera
++++++++++++[#lightcoral] Dependencia de código de terceros
+++++++++++_ Resultando en
++++++++++++[#lightcoral] Sobrecarga de llamadas
@endmindmap
```
Enlace de recurso:

[Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado?  @ Canal UNED](https://canal.uned.es/video/5a6f4bdcb1111f082a8b4619)


# Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación  

Anselmo Peñas Padilla profesor del Departamento de Lenguajes y Sistemas Informáticos, UNED
Javier Vélez Reyes profesor del Departamento Lenguajes y Sistemas Informáticos, UNED
Carlos Celorrio Aguilera becario, UNED
Emilio Julio Lorenzo Galgo profesor del Departamento de Lenguajes y Sistemas Informáticos, UNED
Ana Isabel Ventureira Pedrosa redactora - locutora, CEMAV, UNED

Mapa conceptual con PlantUML:
```plantuml
@startmindmap
+[#turquoise] Evolución de los lenguajes y paradigmas
++[#lightgreen] Lenguaje de programación
+++_ Es una forma de representar
++++[#lavender] Instrucciones de forma entendible para la máquina
++[#FFBBCC] Paradigmas de programación
+++_ Surgen de 
++++[#palegreen]  Problemas complejos
+++_ Son una forma de aproximación a la
++++[#palegreen] resolución de un problema
+++_ Tipos
++++[#palegreen] Estructurada
+++++_ Propone descomponer un
++++++[#powderblue] Problema grande 
+++++++_ en
++++++++[#powderblue] Problemas pequeños
+++++++++[#darkseagreen] Basic
+++++++++[#darkseagreen] C
+++++++++[#darkseagreen] FORTRAN
++++[#palegreen] Funcional
+++++_ Trabaja con
++++++[#powderblue] Funciones Matemáticas
+++++++_ Aplicando
++++++++[#powderblue] Recursividad
+++++++++_ Simulando
++++++++++[#powderblue] Secuencias de control
+++++_ Ejemplos
++++++[#darkseagreen]  Clojure
++++++[#darkseagreen]  ML
++++++[#darkseagreen]  Erlang
++++++[#darkseagreen]  Haskell
++++[#palegreen] Lógica
+++++_ Basada en
++++++[#powderblue] Expresiones lógicas
+++++++_ Utilizando
++++++++[#lavender] Recursividad
++++++++[#lavender] Mecanismos de inferencia
+++++++_ Produce
++++++++[#lavender] Resultados lógicos
+++++++_ Ejemplo
++++++++[#darkseagreen]  Prolog
++++[#palegreen] Distribuida
+++++_ Ejecuta programas
++++++[#powderblue] Grandes y complejos
+++++++_ A través de 
++++++++[#lavender] Distintas máquinas a la vez
++++[#palegreen] Orientado a objetos
+++++_ Comprende una nueva forma de
++++++[#lavender] Organizar el programa
+++++++_ Mediante la
++++++++[#lavender] Construcción de módulos
+++++++++_ Que se
++++++++++[#lavender] Comunican entre ellos
+++++_ Ejemplos
++++++[#darkseagreen] Java
++++++[#darkseagreen] c++
++++++[#darkseagreen] Phyton
++++[#palegreen] Basada en componentes
+++++[#powderblue] Grupos de objetos como componentes
+++++[#powderblue] Reutilización de software
++++[#palegreen] Basada en aspectos
+++++[#powderblue] Base sólida
++++++_ Sobre la que se
+++++++[#lavender] Agregan componentes
++++[#palegreen] Sistemas multiagente
+++++[#powderblue] Unidad autóctona
++++++_ Que necesita
+++++++[#lavender] Alcanzar sus objetivos
++++++_ Puede haber
+++++++[#lavender] Comunicación entre agentes
++[#lightblue] Tendencias
+++[#powderblue] Nivel de abstracción más alto
++++[#lavender] Pensamiento más cercano al humano
+++[#powderblue] Comunicación entre todos los componentes
@endmindmap
```
Un enlace:

[La evolución de los lenguajes y paradigmas de programación @ Canal UNED](https://canal.uned.es/video/5a6f4d3cb1111f082a8b4fbc)


# Hª de los algoritmos y de los lenguajes de programación 

Tendencias en las tecnologías de la información

De Euclides a Java. Historia de los algoritmos y de los lenguajes de programación", es el titulo de un libro escrito por Ricardo Peña Mari en el que se apoya la profesora Araujo para analizar la historia y alguno de los aspectos más importantes de la informática como los algoritmos y los lenguajes de programación. 
 
Mapa conceptual con PlantUML:
```plantuml
@startmindmap
+[#FFBBCC] Algoritmo
++_ Es una
+++[#lavender] Secuencia de pasos
++++[#aquamarine] Logica
++++[#aquamarine] Ordenada
++++[#aquamarine] Finita
++_ Mediante una
+++[#lavender] Entrada
++++_ Produce una
+++++[#lavender] Salida
++_ Debe ser
+++[#lavender] Definido
++++_ A partir de
+++++[#lavender] Las entradas
++++++_ Deben producir
+++++++[#lavender] Las mismas salidas
++_ Se origina en
+++[#lavender] Babilonia
++++_ Utilizado para
+++++[#lavender] Calcular intereses
++_ Para el
+++[#lavender] Cómputo
++++_ Mediante
+++++[#lavender] Lenguajes de programación
++++++_ Agrupados en 
+++++++[#lavender]  Paradigmas de programación
++++++++_ Como
+++++++++[#aquamarine] Lógico
+++++++++[#aquamarine] Funcional
+++++++++[#aquamarine] Orientado a objetos
+++++++++[#aquamarine] Imperativo
+++++++++[#aquamarine] Paralelos
++_ Según su
+++[#lavender] Complejidad
++++[#lavender]  Algoritmos razonables
+++++[#plum]  Los datos se duplican
++++++[#aquamarine] El problema se duplica
++++[#lavender]  Algoritmos no razonables
+++++[#plum]  Un solo dato
++++++[#aquamarine] El problema se duplica

@endmindmap

```

Un enlace:

[Hª de los algoritmos y de los lenguajes de programación @ Canal UNED](https://canal.uned.es/video/5a6f4c8fb1111f082a8b4b9c)
